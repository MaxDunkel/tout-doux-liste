import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {TodoComponent} from './todo/todo.component';

const routes: Routes = [
  {
    path:'todo', //ne pas mettre '/' en début de path, Angular le met tout seul
    component: TodoComponent //un seul objet par route
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
