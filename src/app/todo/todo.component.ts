import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
    public aFaire;
    public inputDonnees;

  constructor() {
    this.aFaire = [];
  }

  ngOnInit() {
  }

  add(){
    this.aFaire.push(this.inputDonnees);
  }
  delete(index){
    this.aFaire.splice(index,1);
  }

}
